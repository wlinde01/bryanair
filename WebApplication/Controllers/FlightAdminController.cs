﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;
using BryanAir.Contracts;
using Status = BryanAir.Contracts.Helpers.Status;

namespace WebApplication.Controllers
{
    public class FlightAdminController : Controller
    {

        // GET: FlightAdmin
        [Authorize]
        public async Task<ActionResult> Index()
        {
            return View(await ServiceClient.GetFlights() );
            //return View(await _db.FlightViewModels.ToListAsync());
        }

        // GET: FlightAdmin/Details/5
        [Authorize]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            FlightViewModel flightViewModel = await ServiceClient.GetFlightById( (int) id);
            if (flightViewModel == null)
            {
                return HttpNotFound();
            }
            return View(flightViewModel);
        }

        // GET: FlightAdmin/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: FlightAdmin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> Create([Bind(Include = "Id,AirlineCode,AirlineName,FlightNumber,DepartureLocation,DepartureDayOfTheWeek,DepartureTime,ArrivalLocation,ArrivalDayOfTheWeek,ArrivalTime,FatCatClassTicketPrice,CattleClassTicketPrice,Status,Created")] FlightViewModel flightViewModel)
        {
            if (!ModelState.IsValid) return View(flightViewModel);

            var success = await ServiceClient.CreateFlight(flightViewModel);
            if (success) return RedirectToAction("Index");

            return View(flightViewModel);
        }

        // GET: FlightAdmin/Edit/5
        [Authorize]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FlightViewModel flightViewModel = await ServiceClient.GetFlightById((int)id);
            if (flightViewModel == null)
            {
                return HttpNotFound();
            }
            return View(flightViewModel);
        }

        // POST: FlightAdmin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> Edit([Bind(Include = "Id,AirlineCode,AirlineName,FlightNumber,DepartureLocation,DepartureDayOfTheWeek,DepartureTime,ArrivalLocation,ArrivalDayOfTheWeek,ArrivalTime,FatCatClassTicketPrice,CattleClassTicketPrice,Status,Created")] FlightViewModel flightViewModel)
        {
            if (!ModelState.IsValid) return View(flightViewModel);
            
            var success = await ServiceClient.UpdateFlight(flightViewModel);
            if (success) return RedirectToAction("Index");

            return View(flightViewModel);
        }

        // GET: FlightAdmin/Cancel/5
        [Authorize]
        public async Task<ActionResult> Cancel(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            FlightViewModel flightViewModel = await ServiceClient.GetFlightById((int)id);
            if (flightViewModel == null)
            {
                return HttpNotFound();
            }
            return View(flightViewModel);
        }

        // POST: FlightAdmin/Delete/5
        [HttpPost, ActionName("Cancel")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> CencelConfirmed(int id)
        {
            var flightViewModel = await ServiceClient.GetFlightById(id);
            await ServiceClient.CancelFlight(flightViewModel);
            return RedirectToAction("Index");
        }


    }
}
