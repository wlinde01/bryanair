﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class ItineraryController : Controller
    {
        


        [Authorize]
        public async Task<ActionResult> Index()
        {
            var userId = User.Identity.GetUserId();
            var itineraries = ServiceClient.GetUserItineraries(userId);
            return View(itineraries);
        }

        [Authorize]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItineraryViewModel itineraryViewModel = await ServiceClient.GetItineraryById((int) id);
            if (itineraryViewModel == null)
            {
                return HttpNotFound();
            }
            return View(itineraryViewModel);
        }

        [Authorize]
        public ActionResult Checkout(int id)
        {
            return RedirectToAction("Index", "Checkout", new {itineraryId = id});
        }

        [Authorize]
        public async Task<ActionResult> Cancel(int id)
        {
            var itinerary = await ServiceClient.GetItineraryById(id);
            var updated = await ServiceClient.UpdateItineraryStatusToCancelled(itinerary);

            if (!updated) ModelState.AddModelError("Badness", "We could not cancel your booking, you'll just have to go now.");

            var itineraries = ServiceClient.GetUserItineraries(User.Identity.GetUserId());
            ModelState.AddModelError("Goodness", "Booking Cancelled!");
            return View("Index", itineraries);
        }
    }
}
