﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using BryanAir.Contracts;
using Microsoft.AspNet.Identity;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    //ToDo: Kill off this bad idea... 
    public class CurrentItineraryController : Controller
    {
        public ActionResult CurrentItinerary()
        {
            if (User.Identity.IsAuthenticated)
            {
                var currentItineraryViewModel = ServiceClient.GetUserCurrentItinerary(User.Identity.GetUserId());
                return PartialView("_CurrentItinerary", currentItineraryViewModel);
            }
            
            return PartialView("_CurrentItinerary");
        }
    }
}
