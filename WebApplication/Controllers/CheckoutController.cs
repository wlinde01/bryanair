﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using WebApplication.Models;
using System.Threading.Tasks;
using BryanAir.Contracts.Helpers;

namespace WebApplication.Controllers
{
    public class CheckoutController : Controller
    {
        // GET: Checkout
        [HttpGet]
        [Authorize]
        public async Task<ActionResult>Index(int itineraryId)
        {
            return View("Payment", await GetModel(itineraryId));
        }


        [HttpPost]
        [Authorize]
        public async Task<ActionResult> SaveParticulars([Bind(Include = "ItineraryId,UserParticularId,creditCardNumber,creditCardExpirationDate")] int itineraryId, string creditCardNumber, string creditCardExpirationDate, int userParticularId)
        {
            // By this point in the journey, a user particular must exist, so update credit card info only

            if  (!(creditCardNumber.Length > 0 && long.Parse(creditCardNumber)%int.Parse(creditCardExpirationDate) == 0))
                    ModelState.AddModelError("Badness", "A valid Credit Card number is divisable by it's expriation date.");

            if (ModelState.IsValid)
            {
                var isUpdateSuccessfull = await ServiceClient
                    .UpdateUserCreditcardParticulars(creditCardNumber, creditCardExpirationDate, User.Identity.GetUserId(), userParticularId);
             
                if (isUpdateSuccessfull)
                    return RedirectToAction("Index", new {itineraryId = itineraryId});
            }

            var model = await GetModel(itineraryId);
           
            ModelState.AddModelError("Badness", "Something bad has happened, it does, from time to time...so try again.");
            return View("Payment", model);

            
        }

        private async Task<CheckoutViewModel> GetModel(int itineraryId)
        {
            var itinerary = await ServiceClient.GetItineraryById(itineraryId);
            var userParticulars = ServiceClient.GetUserParticulars(User.Identity.GetUserId()) ?? new UserParticularsViewModel();
            return new CheckoutViewModel {ItineraryViewModel = itinerary, UserParticularsViewModel = userParticulars};
        }


        [HttpPost]
        [Authorize]
        public async Task<ActionResult> Pay(int itineraryId)
        {

            var itinerary = await ServiceClient.GetItineraryById(itineraryId);

            if (itinerary.Status == Status.Cancelled || itinerary.Status == Status.Booked)
            {
                ModelState.AddModelError("Badness", "This booking is already:" + itinerary.Status.ToString());
                return View("Payment", await GetModel(itineraryId));
            }

            var updated =  await ServiceClient.UpdateItineraryStatusToConfirmed(itinerary);

            if (!updated)
            {
                ModelState.AddModelError("Badness", "We could not take your money... but your money is important to us...try again.");
                return View("Payment", await GetModel(itineraryId));
            }

            ModelState.AddModelError("Goodness", "Booking Confirmed!");
            return View("Confirmation", await GetModel(itineraryId));

        }

       
    }
}
