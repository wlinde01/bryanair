// HTTP Client stuff Courtesy of http://www.asp.net/web-api/overview/advanced/calling-a-web-api-from-a-net-client
// Sorry  about this big nasty blob... requires disection and splitting out + lots of refactoring.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using BryanAir.Contracts;
using BryanAir.Contracts.Helpers;
using WebApplication.Models;
using Status = BryanAir.Contracts.Helpers.Status;

namespace WebApplication.Controllers
{


    public static class ServiceClient
    {
        private const string BaseUrl = "https://microsoft-apiapp0885a20f63d142458587a85861cd530b.azurewebsites.net/";
        //public const string BaseUrl = "http://localhost:34015/";

        public static async Task<IList<FlightViewModel>> GetFlights()
        {
            var modelFlights = new List<FlightViewModel>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl); // ToDo move into Abstact class & either lookup host or specify in Web.config
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("api/flights/");

                if (!response.IsSuccessStatusCode) return modelFlights;
                var flights = await response.Content.ReadAsAsync<IList<BryanAir.Contracts.Flight>>();
                modelFlights.AddRange(flights.Select(MapFlightContractToViewModel));
            }

            return modelFlights;
        }

        

        public static async Task<SearchResultsViewModel> Search(SearchOptionsViewModel searchOptions)
        {
            
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl); // ToDo move into Abstact class & either lookup host or specify in Web.config
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.PostAsJsonAsync("api/search/", MapSearchOptionsModelToContract(searchOptions));

                if (!response.IsSuccessStatusCode) return null;
                var searchResponse = await response.Content.ReadAsAsync<SearchResponse>();

                var searchResultsViewModel = new SearchResultsViewModel
                {
                    SearchOptionsViewModel = searchOptions,
                    OutboundResults = new List<FlightResultsViewModel>(),
                    InboundResults = new List<FlightResultsViewModel>()
                };
                searchResultsViewModel.OutboundResults
                    .AddRange(ProduceFlightResultViewModel(searchOptions, searchOptions.DepartureTime, searchResponse.OutboundFlights));

                searchResultsViewModel.InboundResults
                    .AddRange(ProduceFlightResultViewModel(searchOptions, searchOptions.ReturnTime, searchResponse.InboundFlights));

                return searchResultsViewModel;
            }
        }

        private static IEnumerable<FlightResultsViewModel> ProduceFlightResultViewModel(SearchOptionsViewModel searchOptions, DateTime searchFlightDepartureTime, IEnumerable<Flight> flights)
        {
            return flights
                .Select(f => MapFlightContractToFlightResultsViewModel(
                    f,
                    searchOptions.Itinerary.Id,
                    searchOptions.TotalPassengers,
                    searchFlightDepartureTime))
                    
                    // Filter out any Flights that are already on this Itinerary || A bit risky, there is a small chance some one  might book the same "flight" weeks appart...
                    .Where(f => 
                        !searchOptions.Itinerary.Bookings.Select(b => b.Id).ToList().Contains(f.FlightViewModel.Id)
                        && (searchOptions.TotalPassengers <= f.FlightViewModel.AvailableBussinessSeats || searchOptions.TotalPassengers <= f.FlightViewModel.AvailableEcoSeats));
        }

        private static SearchRequest MapSearchOptionsModelToContract(SearchOptionsViewModel searchOptions)
        {
            return new SearchRequest
            {
                ArrivalLocation = searchOptions.ArrivalLocation,
                DepartureLocation = searchOptions.DepartureLocation,
                DepartureTime = searchOptions.DepartureTime,
                ReturnTime = searchOptions.ReturnTime,
                TotalPassengers = searchOptions.TotalPassengers,
                IsReturnFlight = searchOptions.IsReturnFlight,

            
            };
        }


        public static async Task<FlightViewModel> GetFlightById(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl); // ToDo move into Abstact class & either lookup host or specify in Web.config
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync(string.Format("api/flights/{0}",id));

                if (!response.IsSuccessStatusCode) return null; //meh
                var flight = await response.Content.ReadAsAsync<BryanAir.Contracts.Flight>();
                return MapFlightContractToViewModel(flight);
            }
                
        }


        public static async Task<bool> CancelFlight(FlightViewModel flightViewModel)
        {
            flightViewModel.Status = Status.Cancelled;
            return await UpdateFlight(flightViewModel);
        }
        public static async Task<bool> UpdateFlight(FlightViewModel flightViewModel)
        {
            using (var client = new HttpClient())
            {
            
                client.BaseAddress = new Uri(BaseUrl); 
                var response = await client.PutAsJsonAsync("api/flights", MapFlightViewModelToContract(flightViewModel));
                return (response.IsSuccessStatusCode);
            }
        }

        public static async Task<bool> CreateFlight(FlightViewModel flightViewModel)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl);
                var response = await client.PostAsJsonAsync("api/flights", MapFlightViewModelToContract(flightViewModel));
                return (response.IsSuccessStatusCode);
            }
        }

        public static async Task<bool> CreateUserParticulars(RegisterViewModel registerViewModel, string userId)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl);
                var response = await client.PostAsJsonAsync("api/userparticulars", MapRegisterViewModelToContract(registerViewModel, userId));
                return (response.IsSuccessStatusCode);
            }
        }

        public static async Task<bool> UpdateUserCreditcardParticulars(string creditCardNumber, string creditCardExpirationDate, string userId, int userParticularId)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl);
                var creditCardParticular = new UserParticular
                {
                    Id = userParticularId,
                    ReferenceId = new Guid(userId),
                    CreditCardExpirationDate = creditCardExpirationDate,
                    CreditCardNumber = creditCardNumber
                };

                var response = await client.PutAsJsonAsync("api/userparticulars", creditCardParticular);

                return (response.IsSuccessStatusCode);
            }
        }

        public static UserParticularsViewModel GetUserParticulars(string userId)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl); // ToDo move into Abstact class & either lookup host or specify in Web.config
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync(string.Format("api/userparticulars/{0}", userId)).Result;

                if (!response.IsSuccessStatusCode) return null; //meh
                var userParticulars = response.Content.ReadAsAsync<UserParticular>().Result;
                return MapParticularsContractToViewModel(userParticulars);
            }
        }

        private static UserParticularsViewModel MapParticularsContractToViewModel(UserParticular particulars)
        {
            return new UserParticularsViewModel
            {
                AddressLine1 = particulars.AddressLine1,
                AddressLine2 = particulars.AddressLine2,
                CreditCardExpirationDate = particulars.CreditCardExpirationDate,
                Id = particulars.Id,
                CreditCardNumber = particulars.CreditCardNumber,
                Name = particulars.Name,
                Postcode = particulars.Postcode,
                ReferenceId = particulars.ReferenceId
            };
        }

        public static ItineraryViewModel GetUserCurrentItinerary(string userId)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl); // ToDo move into Abstact class & either lookup host or specify in Web.config
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync(string.Format("api/currentitinerary/user/{0}", userId)).Result;

                if (!response.IsSuccessStatusCode) return null; //meh
                var itineraries = response.Content.ReadAsAsync<BryanAir.Contracts.Itinerary>().Result;
                return MapItineraryContractToViewModel(itineraries);
            }
        }


        public static List<ItineraryViewModel> GetUserItineraries(string userId)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl); // ToDo move into Abstact class & either lookup host or specify in Web.config
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync(string.Format("api/itineraries/user/{0}", new Guid(userId))).Result;

                if (!response.IsSuccessStatusCode) return null; //meh
                var itineraries = response.Content.ReadAsAsync <List<BryanAir.Contracts.Itinerary>>().Result;
                return itineraries.Select(MapItineraryContractToViewModel).ToList();
            }
        }

        public static ItineraryViewModel CreateNewItinerary(string userId = null )
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl); // ToDo move into Abstact class & either lookup host or specify in Web.config
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var newItineraryToCreate = new Itinerary
                {
                    Status = (int)Status.New,
                    Bookings =  new List<Booking>()
                };
                if (userId != null) newItineraryToCreate.ReferenceId = new Guid(userId);

                HttpResponseMessage response = client.PostAsJsonAsync("api/itineraries", newItineraryToCreate).Result;
                var createdItinerary = response.Content.ReadAsAsync<Itinerary>().Result;

                return new ItineraryViewModel
                {
                    Id = createdItinerary.Id,
                    Bookings = new List<BookingViewModel>(),
                    Status = (Status) createdItinerary.Status
                };
            }
        }


        public static async Task<bool> UpdateItineraryStatusToReserved(ItineraryViewModel itinerary)
        {
            itinerary.Status = Status.Reserved;
            return await UpdateItinerary(itinerary);
        }

        public static async Task<bool> UpdateItineraryStatusToConfirmed(ItineraryViewModel itinerary)
        {
            itinerary.Status = Status.Booked;
            return await UpdateItinerary(itinerary);
        }

        public static async Task<bool> UpdateItineraryStatusToCancelled(ItineraryViewModel itinerary)
        {
            itinerary.Status = Status.Cancelled;
            return await UpdateItinerary(itinerary);
        }        
        public static async Task<bool> UpdateItinerary(ItineraryViewModel itinerary)
        {
            //var itinerary = 
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl);
                var response = await client.PutAsJsonAsync("api/itineraries/", MapItineraryViewModelToContract(itinerary));
                return (response.IsSuccessStatusCode);
            }


        }

        private static Itinerary MapItineraryViewModelToContract(ItineraryViewModel itinerary)
        {
            return new Itinerary
            {
                Id = itinerary.Id,
                ReferenceId = new Guid(itinerary.UserId),
                Bookings = itinerary.Bookings.Select(MapBookingViewModelToContract).ToList(),
                Status = (int) itinerary.Status
            };
        }

        private static Booking MapBookingViewModelToContract(BookingViewModel booking)
        {
            return new Booking
            {
                DepartureDate = booking.DepartureDate,
                Direction = booking.Direction,
                PriceLevel = booking.PriceLevel,
                Flight = MapFlightViewModelToContract(booking.Flight),
                Id = booking.Id,
                Price = booking.Price,
                TotalPassengers = booking.TotalPassengers
            };
        }


        public static async Task<BookingViewModel> BookFlight(int flightId, int itinireraryId, int passengers, DateTime departureDate, FlightDirection direction, PriceLevel priceLevel)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl); // ToDo move into Abstact class & either lookup host or specify in Web.config
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = 
                    client.PutAsJsonAsync(string.Format("api/itineraries/{0}", itinireraryId),
                                        new NewBookingRequest
                                        {
                                            DepartureDate = departureDate,
                                            FlightId = flightId,
                                            TotalPassengers = passengers,
                                            PriceLevel = priceLevel,
                                            Direction = direction
                                        }).Result;

                var newBooking = response.Content.ReadAsAsync<Booking>().Result;

                return MapBookingContractToViewModel(newBooking);

            }

        }

        private static BookingViewModel MapBookingContractToViewModel(Booking newBooking)
        {
            return new BookingViewModel
            {
                Id = newBooking.Id,
                TotalPassengers = newBooking.TotalPassengers,
                DepartureDate = newBooking.DepartureDate,
                Price = newBooking.Price,
                PriceLevel = newBooking.PriceLevel,
                Direction = newBooking.Direction,
                Flight = MapFlightContractToViewModel(newBooking.Flight)
            };
        }


        public static async Task<ItineraryViewModel> GetItineraryById(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl); // ToDo move into Abstact class & either lookup host or specify in Web.config
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync(string.Format("api/itineraries/{0}", id));

                if (!response.IsSuccessStatusCode) return null; //meh

                var itinerary = await response.Content.ReadAsAsync<Itinerary>();
                return MapItineraryContractToViewModel(itinerary);

            }
        }


        private static ItineraryViewModel MapItineraryContractToViewModel(Itinerary itinerary)
        {
            return new ItineraryViewModel
            {
                Id = itinerary.Id,
                Status = (Status) itinerary.Status,
                UserId = itinerary.ReferenceId.ToString(),
                Bookings = itinerary.Bookings.Select(MapBookingContractToViewModel).ToList()
            };
        }

        private static UserParticular MapRegisterViewModelToContract(RegisterViewModel registerViewModel, string userId)
        {
            return new UserParticular
            {
                ReferenceId = new Guid(userId),
                Name = registerViewModel.Name,
                AddressLine1 = registerViewModel.AddressLine1,
                AddressLine2 = registerViewModel.AddressLine2,
                Postcode = registerViewModel.Postcode,
                CreditCardExpirationDate = registerViewModel.CreditCardExpirationDate,
                CreditCardNumber = registerViewModel.CreditCardNumber
            };
        }


        private static FlightViewModel MapFlightContractToViewModel(Flight flight)
        {
            return new FlightViewModel
            {
                AirlineCode = flight.AirlineCode,
                AirlineName = flight.AirlineName,
                ArrivalDayOfTheWeek = (DayOfWeek)flight.ArrivalDayOfTheWeek,
                ArrivalLocation = flight.ArrivalLocation,
                ArrivalTime = flight.ArrivalTime,
                CattleClassTicketPrice = flight.CattleClassTicketPrice,
                Created = flight.Created,
                DepartureDayOfTheWeek = (DayOfWeek)flight.DepartureDayOfTheWeek,
                DepartureLocation = flight.DepartureLocation,
                DepartureTime = flight.DepartureTime,
                Id = flight.Id,
                Status = (Status)flight.Status,
                FatCatClassTicketPrice = flight.FatCatClassTicketPrice,
                FlightNumber = flight.FlightNumber,
                // Magic Numbers
                AvailableBussinessSeats = 10 - flight.BookedSeatsBussiness,
                AvailableEcoSeats = 90 - flight.BookedSeatsEco
            };
        }

        private static FlightResultsViewModel MapFlightContractToFlightResultsViewModel(Flight flight, int itineraryId, int passengers,  DateTime departureDate)
        {
            return new FlightResultsViewModel
            {
                FlightViewModel = MapFlightContractToViewModel(flight),
                SearchDepartureDate = departureDate,
                Passengers = passengers,
                ItinireraryId = itineraryId
            };
        }


        private static Flight MapFlightViewModelToContract(FlightViewModel flight)
        {
            return new Flight
            {
                AirlineCode = flight.AirlineCode,
                AirlineName = flight.AirlineName,
                ArrivalDayOfTheWeek = (int) flight.ArrivalDayOfTheWeek,
                ArrivalLocation = flight.ArrivalLocation,
                ArrivalTime = flight.ArrivalTime,
                CattleClassTicketPrice = flight.CattleClassTicketPrice,
                Created = flight.Created,
                DepartureDayOfTheWeek = (int) flight.DepartureDayOfTheWeek,
                DepartureLocation = flight.DepartureLocation,
                DepartureTime = flight.DepartureTime,
                Id = flight.Id,
                Status = (BryanAir.Contracts.Helpers.Status) flight.Status,
                FatCatClassTicketPrice = flight.FatCatClassTicketPrice,
                FlightNumber = flight.FlightNumber
            };
        }



    }
}
