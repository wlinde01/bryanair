﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BryanAir.Contracts.Helpers;
using Microsoft.AspNet.Identity;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class SearchController : Controller
    {
        public ActionResult Search()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Search([Bind(Include = "DepartureLocation,DepartureTime,ReturnTime,ArrivalLocation,TotalPassengers,IsReturnFlight,ItineraryId")] SearchOptionsViewModel searchOptions)
        {
            if (!ModelState.IsValid) return View(searchOptions);
            if (searchOptions.ItineraryId == null)
            {
                searchOptions.Itinerary = ServiceClient.CreateNewItinerary(User.Identity.IsAuthenticated ? User.Identity.GetUserId() : null);
            }
            else searchOptions.Itinerary = await ServiceClient.GetItineraryById((int) searchOptions.ItineraryId);

            var response = await ServiceClient.Search(searchOptions);
            return View("SearchResults", response);
        }

        [HttpGet]
        public async Task<ActionResult> Search(int? itineraryId)
        {
            if (itineraryId == null) return Search();
            var itinerary = await ServiceClient.GetItineraryById((int) itineraryId);
            var searchOptions = new SearchOptionsViewModel {Itinerary = itinerary};
            return View("Search", searchOptions);
        }

        [HttpGet]
        public async Task<ActionResult> Book(int flightId, int itinireraryId, int passengers, DateTime departureDate, int direction, int priceLevel)
        {
            var response = await ServiceClient.BookFlight(flightId, itinireraryId, passengers, departureDate, (FlightDirection) direction, (PriceLevel) priceLevel);
            return PartialView("_BookedFlight", response);
        }

        public async Task<ActionResult> Confirm(int? itineraryid)
        {
            if (itineraryid == null) RedirectToAction("Search");
            //ToDo Start Timer (2 mins) // for statelessness, really need this to persist via timestamp
            
            var itinerary = await ServiceClient.GetItineraryById((int) itineraryid);
            var updated =  await ServiceClient.UpdateItineraryStatusToReserved(itinerary);
         
            if (updated) return RedirectToAction("Details", "Itinerary", new { id = itineraryid });

            ModelState.AddModelError("Badness", "A problem: Could not reserve your flight... but your call is important to us...please hold or try again.");
            return View("Search");
        }
    }
}
