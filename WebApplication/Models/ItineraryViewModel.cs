﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BryanAir.Contracts.Helpers;

namespace WebApplication.Models
{
    public class ItineraryViewModel
    {
        public int Id { get; set; }
        public Status Status { get; set; }
        public string UserId { get; set; }
        public IList<BookingViewModel> Bookings { get; set; }
        public decimal TotalPrice
        {
            get { return Bookings.Sum(b => b.Price*b.TotalPassengers); }
        }

        public string Description
        {
            get
            {
                var firstFl = Bookings.OrderBy(b => b.DepartureDate).FirstOrDefault();
                var lastFl = Bookings.OrderByDescending(b => b.DepartureDate).FirstOrDefault();

                if (firstFl != null && lastFl != null)
                    return (firstFl != lastFl)
                        ? string.Format("{2} | {0} -> {1}", firstFl.Flight.DepartureLocation, lastFl.Flight.DepartureLocation, firstFl.DepartureDate)
                        : string.Format("{2} | {0} -> {1}", firstFl.Flight.DepartureLocation, firstFl.Flight.ArrivalLocation, firstFl.DepartureDate);
                return "This booking has issues.";
            }
        }

        public bool IsCancelable
        {
            get
            {
                return Status == Status.New  || Status == Status.Reserved;
            }
        }
    }

    public class BookingViewModel
    {
        public int Id { get; set; }
        public FlightViewModel Flight { get; set; }
        public DateTime DepartureDate { get; set; }
        public int TotalPassengers { get; set; }
        public decimal Price { get; set; }
        public FlightDirection Direction { get; set; }
        public PriceLevel PriceLevel { get; set; }
    }
}
