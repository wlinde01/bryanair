﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models
{
    public class SearchOptionsViewModel
    {
        [Required]
        [Display(Name = "Flying From")]
        public string DepartureLocation { get; set; }

        [Required]
        [Display(Name = "Departure Date/Time")]
        public DateTime DepartureTime { get; set; }

        [Display(Name = "Return Flight?")]
        public bool IsReturnFlight {get; set;}

        [Required]
        [Display(Name = "Flying To")]
        public string ArrivalLocation { get; set; }

        [Display(Name = "Return Date/Time")]
        public DateTime ReturnTime { get; set; }

        public int? ItineraryId { get; set; }
        
        [Required]
        [Range(1,90)] 
        [Display(Name = "Passengers")]
        public int TotalPassengers { get; set; }

        public ItineraryViewModel Itinerary { get; set; }
    }

    public class SearchResultsViewModel
    {
        public SearchOptionsViewModel SearchOptionsViewModel { get; set; }
        public List<FlightResultsViewModel> OutboundResults { get; set; }
        public List<FlightResultsViewModel> InboundResults { get; set; }

    }
}
