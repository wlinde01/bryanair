﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace WebApplication.Models
{
    public class UserParticularsViewModel
    {
        [Key]
        public int Id { get; set; }

        [System.Web.Mvc.HiddenInput(DisplayValue = false)]
        public Guid ReferenceId { get; set; }

        [Required]
        public string Name { get; set; }
        
        [Required]
        [Display(Name = "Address Line 1")]
        public string AddressLine1 { get; set; }
        [Display(Name = "Address Line 2")]
        public string AddressLine2 { get; set; }
        
        [Required]
        public string Postcode { get; set; }
        
        [RegularExpression("([0-9\\s]+)", ErrorMessage = "Please enter a valid Card Number")]
        [StringLength(19, ErrorMessage = "The {0} must contain exactly {2} digits.", MinimumLength = 16)]
        [Display(Name = "Credit Card Number")]
        public string CreditCardNumber { get; set; } // 16 digits
        
        [RegularExpression("([0-9]?[0-9][0-9]?[0-9])", ErrorMessage = "Please enter a valid Expiration date format (mmyy)")]
        [Display(Name = "Credit Card Expiration Date (mm/yy)")]
        public string CreditCardExpirationDate { get; set; }
    }
}
