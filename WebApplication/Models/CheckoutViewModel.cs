﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace WebApplication.Models
{
    public class CheckoutViewModel
    {
/*        public int Id { get; set; }*/
        public UserParticularsViewModel UserParticularsViewModel { get; set; }
        public ItineraryViewModel ItineraryViewModel { get; set; }
        public int ItineraryId { get { return ItineraryViewModel.Id; } }

        public bool RequiresCreditCardInfo
        {
            get { return
                    (UserParticularsViewModel.CreditCardNumber == null
                    || UserParticularsViewModel.CreditCardExpirationDate == null); }
        }

        public decimal Total
        {
            get { return ItineraryViewModel.Bookings.Sum(b => (b.Price*b.TotalPassengers)); }
        }

        public string Tickets
        {
            get
            {
                // Not pretty, but build a giant ticket with all flights info make it pretty / partial view etc if time.

                var tickets = ItineraryViewModel.Bookings.Select(GetFlightTicket);
                return tickets.Aggregate((t1,t2) => t1 + " | " + t2);
            }
        }

        private string GetFlightTicket(BookingViewModel booking)
        {
            var flight = booking.Flight;
            return string.Format(CultureInfo.InvariantCulture, "{0}-{1}-{2}-{3}",
                flight.AirlineCode,
                flight.FlightNumber,
                // ToDo: Oops, login name is email ...use registered name, if null, use user GUID
                UserParticularsViewModel.Name ?? UserParticularsViewModel.ReferenceId.ToString(),
                UserParticularsViewModel.Id
                );
        }

    }
}
