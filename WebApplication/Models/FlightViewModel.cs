using System;
using System.ComponentModel.DataAnnotations;
using System.Security.AccessControl;
using BryanAir.Contracts.Helpers;


namespace WebApplication.Models
{
    public class FlightViewModel
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Airline Code (IATA)")]
        [StringLength(2, ErrorMessage = "The {0} must be exactly {2} characters long.", MinimumLength = 2)]
        public string AirlineCode { get; set; } // see IATA  https://en.wikipedia.org/wiki/List_of_airline_codes
        
        [Display(Name = "Airline")]
        public string AirlineName { get; set; } // see IATA  https://en.wikipedia.org/wiki/List_of_airline_codes
        
        [Display(Name = "Flight Number")]
        [Range(1,999, ErrorMessage = "The flight number must be between 1 and 999") ]
        public int FlightNumber { get; set; } //a 3-digit number, prefixed with 0�s  (Use ViewModel to display etrx digits)

        [Display(Name = "Departure Location")]
        public string DepartureLocation { get; set; }
        [Display(Name = "Departure Day of Week")]
        public DayOfWeek DepartureDayOfTheWeek { get; set; }
        [Display(Name = "Departure Time")]
        public TimeSpan DepartureTime { get; set; }

        [Display(Name = "Arrival Location")]
        public string ArrivalLocation { get; set; }
        [Display(Name = "Arrival Day of Week")]
        public DayOfWeek ArrivalDayOfTheWeek { get; set; }
        [Display(Name = "Arrival Time")]
        public TimeSpan ArrivalTime { get; set; }

        [Display(Name = "First Class Price")]
        public decimal FatCatClassTicketPrice { get; set; }
        [Display(Name = "Economy Class Price")]
        public decimal CattleClassTicketPrice { get; set; } 

        public Status Status { get; set; } // 0 = normal ; 1 = cancelled ... 2 = delayed etc. (should be an enum really.. think MVP!!)

        public DateTime Created { get; set; }
        public int AvailableBussinessSeats { get; set; }
        public int AvailableEcoSeats { get; set; }
    }

    public class FlightResultsViewModel 
    {
        public FlightViewModel FlightViewModel { get; set; }
        public int ItinireraryId { get; set; }
        public int Passengers { get; set; }
        public DateTime SearchDepartureDate { get; set; }

        public int TimeDiffirenceFromSearch
        {
            get { return (FlightViewModel.DepartureTime - SearchDepartureDate.TimeOfDay).Hours; }
        }


    }
}
