Bryan Air.

Appologies for the extreme sparse documentation, time has been a bit tight.
Also, a fair amount of hackage and a few shortcute induced anti patterns. 


# Application Architecture.

The application consists of the following tiers:

# Entity Framework ORM [Configured to use MySQL currently]

## Web Service tier
	- VS Project: BryanAir.WebService (Namespace: BryanAir.Web)
	- This is build on MS Web Api with Repository Service tier connecting it to the DB via EF.
	- The web service will produce all the std HTTP content dispositions, based on an exposed Contracts project (BryanAir.Contracts). 
	- As far as possible and as time permitted, it is more or less a RESTfull web serice tier.
	- A bit of IOC and testing here, but very limited due to time constraints.

## BryanAir.Contracts: 
	- This is a convenience project, also used by the consuming client Web Application (this is only a convenience, a web service)
	- Consists of POCO C# objects and used for serialisation / deserialisation.
	- As a convenience, some helper enums are also located in here. (Booking status etc.)

## Web Application tier
	- VS Project: BryanAir.WebApplication (Namespace: WebApplication)
	- This is an MVC 5 based website and contains both the user and administration sections.
	- As a convenience, a single database is shared with the Web Services tier, but the only direct access to it, is for User Authentication. This could easily be split out by changing configs. All busssiness data is communicated via the Web service APIs.


# To Run
- It is a Visual Studio 2013 Project, so use that if you'd like to run it.
- A working MySQL server with admin / root creds.
- Place those creds in both (Web Service & Web Application) web.cofig files: 
	- <add name="DefaultConnection" providerName="MySql.Data.MySqlClient" connectionString="server=localhost;UserId=bryanair;Password=bryanair;database=bryanair;CharSet=utf8;Persist Security Info=True" />
- Enabable Automatic migrations if needed in the Migrations folders (File: Configuration.cs)
	- AutomaticMigrationsEnabled = true;
- Run Schema creation using the Package Manager Console:
	> update-database -Verbose -project BryanAir.WebApplication
	> update-database -Verbose -project BryanAir.WebServices
	(Some test data will be inserted)

- Set BryanAir.WebApplication as the startup project, and run.
- Contact me here: williamrlinden@gmail.com

Known shortcommings:
- Timer functionality missing (there is no 2 min timeout).
- A few number formattings and potential validations are lacking.
- A bug that causes a bit of trouble on Inbound flight booking numbers.
- UI has not had much love.



