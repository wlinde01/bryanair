﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using BryanAir.Contracts;
using BryanAir.Contracts.Helpers;
using BryanAir.Web.DomainModel;
using BryanAir.Web.Flights;
using Booking = BryanAir.Contracts.Booking;
using Itinerary = BryanAir.Contracts.Itinerary;


namespace BryanAir.Web.Itineraries
{
    public class ItineraryController : ApiController
    {
        private readonly IItineraryService _itineraryService;


        public ItineraryController(IItineraryService itineraryService)
        {
            _itineraryService = itineraryService;
        }


        [HttpPost]
        [Route("api/itineraries")]
        public IHttpActionResult AddItinerary(Itinerary itinerary)
        {
            var insertedItinerary = _itineraryService.CreateItinerary(itinerary).Result;
            if (insertedItinerary == null) return BadRequest();
            return  Ok(insertedItinerary);
        }

        [HttpPut]
        [Route("api/itineraries/{itineraryId}")]
        public IHttpActionResult AddBooking(int itineraryId, NewBookingRequest booking)
        {
            Booking createdBooking = _itineraryService.AddBooking(itineraryId, booking);
            if (createdBooking == null) return BadRequest();
            return Ok(createdBooking);
        }

        [HttpPut]
        [Route("api/itineraries")]
        public IHttpActionResult UpdateItinerary(Itinerary itinerary)
        {
            if (_itineraryService.UpdateItinerary(itinerary).Result)
                return Ok(itinerary);
            return BadRequest();
        }


        [HttpGet]
        [Route("api/itineraries/{id}")]
        public IHttpActionResult GetItinerary(int id)
        {
            var itinerary = _itineraryService.GetItinerary(id);
            if (itinerary == null) return NotFound();
            return Ok(itinerary);
        }


        [HttpGet]
        [Route("api/itineraries/user/{userId}")]
        public IHttpActionResult GetUserItineraries(Guid userId)
        {
            var userItineraries = _itineraryService.GetUserItineraries(userId);
           // if (userItineraries.Count <= 0) return NotFound();
            return Ok(userItineraries);
        }


        [HttpGet]
        [Route("api/currentitinerary/user/{userId}")]
        public IHttpActionResult GetUserCurrentItinerary(Guid userId)
        {
            var itinerary = _itineraryService.GetCurrentItinerary(userId);
            if (itinerary == null) return NotFound();
            return Ok(itinerary);
        }

    }


    public class ItineraryService : IItineraryService
    {
        BryanAirDbContext _dbContext = new BryanAirDbContext();
        
        public async Task<Itinerary> CreateItinerary(Itinerary itinerary)
        {
            var itineraryModel = MapContractToDomainModel(itinerary);
            _dbContext.Itineraries.Add(itineraryModel);
            var i = await _dbContext.SaveChangesAsync();
            return i <= 0 ? null : MapDomainModelToContract(itineraryModel);
        }

        public Itinerary GetItinerary(int id)
        {
            var itinerary = _dbContext.Itineraries
                .Where(i => i.Id == id)
                .Include(i => i.Bookings.Select(b => b.Flight))
                .FirstOrDefault();

            if (itinerary == null) return null;
            return MapDomainModelToContract(itinerary);
        }

        public async Task<bool> UpdateItinerary(Itinerary itinerary)
        {
            _dbContext.Entry(MapContractToDomainModel(itinerary)).State = EntityState.Modified;;
            var i = await _dbContext.SaveChangesAsync();
            return i > 0;
        }

        public IList<Itinerary> GetUserItineraries(Guid userId)
        {
            //var itineraries = _dbContext.Itineraries.Where(i => i.ReferenceId == userId).ToList();
            var itineraries = _dbContext.Itineraries
                .Where(i => i.ReferenceId == userId)
                .Include(i => i.Bookings.Select(b => b.Flight));
            
            return itineraries == null ? null : itineraries.Select(MapDomainModelToContract).ToList();
        }

        public Itinerary GetCurrentItinerary(Guid userId)
        {
            var itinerary = _dbContext.Itineraries
                .Where(i => i.ReferenceId == userId && i.Status == (int) Status.Reserved)
                .Include(i => i.Bookings.Select(b => b.Flight))
                .FirstOrDefault();

            if (itinerary == null) return null;
            return MapDomainModelToContract(itinerary);
        }

        public Booking AddBooking(int itineraryId, NewBookingRequest booking)
        {
            var itinerary = _dbContext.Itineraries.Find(itineraryId);
            var flight = _dbContext.Flights.Find(booking.FlightId);
            if (itinerary == null || flight == null) return null;

            var newBooking = new DomainModel.Booking {
                DepartureDate = booking.DepartureDate, 
                Flight = flight, 
                TotalPassengers = booking.TotalPassengers,
                Price = booking.PriceLevel == PriceLevel.Economy ? flight.CattleClassTicketPrice : flight.FatCatClassTicketPrice,
                Direction = booking.Direction,
                PriceLevel = booking.PriceLevel
            };

            itinerary.Bookings.Add(newBooking);
            var i = _dbContext.SaveChangesAsync().Result;
            if (i <= 0) return null;
            return MapBookingDomainModelToContract(newBooking);


        }

        private Booking MapBookingDomainModelToContract(DomainModel.Booking booking)
        {
            return new Booking
            {
                Id = booking.Id,
                DepartureDate = booking.DepartureDate,
                Flight = FlightService.MapFlightModelToContract(booking.Flight),
                TotalPassengers = booking.TotalPassengers,
                Direction = booking.Direction,
                PriceLevel = booking.PriceLevel,
                Price = booking.Price
            };
        }

        //ToDo : mapper ... and / or Generics
        private Itinerary MapDomainModelToContract(DomainModel.Itinerary itinerary)
        {
            var itineraryModel = new Itinerary
            {
                Id = itinerary.Id,
                ReferenceId = itinerary.ReferenceId,
                Status = itinerary.Status,
                Bookings = new List<Contracts.Booking>()
            };

            foreach (var booking in itinerary.Bookings)
            {
                itineraryModel.Bookings.Add(new Contracts.Booking
                {
                    DepartureDate = booking.DepartureDate,
                    Flight = FlightService.MapFlightModelToContract(booking.Flight), // mapp this
                    Id = booking.Id,
                    TotalPassengers = booking.TotalPassengers,
                    Direction = booking.Direction,
                    Price = booking.Price,
                    PriceLevel = booking.PriceLevel
                });
                
            }
            return itineraryModel;
        }

        private DomainModel.Itinerary MapContractToDomainModel(Itinerary itinerary)
        {
            var itineraryModel = new DomainModel.Itinerary
            {
                Id = itinerary.Id,
                ReferenceId = itinerary.ReferenceId,
                Status = (int) itinerary.Status,
                Bookings = new List<DomainModel.Booking>()
            };

            foreach (var booking in itinerary.Bookings)
            {
                itineraryModel.Bookings.Add(new DomainModel.Booking
                {
                    DepartureDate = booking.DepartureDate,
                    Flight = FlightService.MapContractToModel(booking.Flight), // mapp this
                    Id = booking.Id,
                    TotalPassengers = booking.TotalPassengers
                });
            }
            return itineraryModel;
        }
    }

    public interface IItineraryService
    {
        Task<Itinerary> CreateItinerary(Itinerary itinerary);
        Itinerary GetItinerary(int id);
        Task<bool> UpdateItinerary(Itinerary itinerary);
        IList<Itinerary> GetUserItineraries(Guid userId);
        Itinerary GetCurrentItinerary(Guid userId);
        Booking AddBooking(int itineraryId, NewBookingRequest booking);
    }

    
}
