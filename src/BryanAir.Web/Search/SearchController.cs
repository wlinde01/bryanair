﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using BryanAir.Contracts;
using BryanAir.Contracts.Helpers;
using BryanAir.Web.DomainModel;
using BryanAir.Web.Flights;
using Flight = BryanAir.Contracts.Flight;

namespace BryanAir.Web.Search
{
    public interface ISearchService
    {
        SearchResponse Find(SearchRequest searchRequest);
    }


    public class SearchService : ISearchService
    {
        private readonly BryanAirDbContext _dbContext = new BryanAirDbContext();

        private int AvailableSeats(DateTime dateOfFlightSearch, int flightId, PriceLevel priceLevel)
        {
            var itineraries = _dbContext.Itineraries
                .Where(i => i.Status == (int) Status.Reserved || i.Status == (int) Status.Booked)
                .Include(i => i.Bookings.Select(b => b.Flight))
                ;

            var bookedSeats = 0;
            foreach (var itinerary in itineraries)
            {
                bookedSeats += itinerary.Bookings
                    .Where(b => b.PriceLevel == priceLevel && b.Flight.Id == flightId && b.DepartureDate.Date == dateOfFlightSearch.Date)
                    .Sum(booking => booking.TotalPassengers);
            }

            return bookedSeats;
        }

        public SearchResponse Find(SearchRequest searchRequest)
        {
            var outboundFlights = GenerateOutboundFlights(searchRequest);


            var inBoundFlights = new List<Flight>();
            // Return's Departure is Search req's Arrival
            if (searchRequest.IsReturnFlight)
            {
                inBoundFlights = GenerateInBoundFlights(searchRequest, inBoundFlights);
            }

            return new SearchResponse{ OutboundFlights = outboundFlights, InboundFlights = inBoundFlights};
        }

        private List<Flight> GenerateInBoundFlights(SearchRequest searchRequest, List<Flight> inBoundFlights)
        {
            inBoundFlights = _dbContext.Flights
                .Where(f => f.DepartureLocation == searchRequest.ArrivalLocation
                            && f.ArrivalLocation == searchRequest.DepartureLocation
                            && f.DepartureDayOfTheWeek == (int) searchRequest.ReturnTime.DayOfWeek
                )
                //.Select(f => MapFlightModelToContract(f, searchRequest.DepartureTime))
                .Select(FlightService.MapFlightModelToContract)
                .ToList();

            foreach (var inBoundFlight in inBoundFlights)
            {
                inBoundFlight.BookedSeatsBussiness = AvailableSeats(searchRequest.ReturnTime, inBoundFlight.Id, PriceLevel.Bussiness);
                inBoundFlight.BookedSeatsEco = AvailableSeats(searchRequest.ReturnTime, inBoundFlight.Id, PriceLevel.Economy);
            }
            return inBoundFlights;
        }

        private List<Flight> GenerateOutboundFlights(SearchRequest searchRequest)
        {
            var outboundFlights = _dbContext.Flights
                .Where(f => f.DepartureLocation == searchRequest.DepartureLocation
                            && f.ArrivalLocation == searchRequest.ArrivalLocation
                            && f.DepartureDayOfTheWeek == (int) searchRequest.DepartureTime.DayOfWeek
                )
                .Select(FlightService.MapFlightModelToContract)
                .ToList();

            foreach (var outBoundFlight in outboundFlights)
            {
                outBoundFlight.BookedSeatsBussiness = AvailableSeats(searchRequest.DepartureTime, outBoundFlight.Id, PriceLevel.Bussiness);
                outBoundFlight.BookedSeatsEco = AvailableSeats(searchRequest.DepartureTime, outBoundFlight.Id, PriceLevel.Economy);
            }
            return outboundFlights;
        }

    }


    public class SearchController : ApiController
    {

        private readonly ISearchService _searchService;

        public SearchController(ISearchService searchService)
        {
            _searchService = searchService;
        }



        [HttpPost]
        [Route("api/search")]
        public IHttpActionResult Search(SearchRequest searchRequest)
        {

            var flightResults =  _searchService.Find(searchRequest);
            return Ok(flightResults);
          
        }

    }

    
}
