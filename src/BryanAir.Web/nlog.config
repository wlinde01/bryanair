﻿<?xml version="1.0" encoding="utf-8" ?>

<nlog
  xmlns="http://www.nlog-project.org/schemas/NLog.xsd"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  throwExceptions="true"
  autoReload="true">

  <extensions>
    <add assembly="JustEat.LoggingUtilities" />
  </extensions>

<!-- variables that take data from config-expansion are separated up at the top to keep the rest of the file standard across apps -->
  <variable name="feature" value="bryanair" />
  <variable name="instance_number" value="001" />

  <variable name="exception_summary" value="${onexception:EXCEPTION OCCURRED\: ${exception:format=ShortType,Message}" />
  <variable name="exceptions_detail" value="${exception:format=Type,Message,ShortType,ToString,Method,Data,StackTrace:maxInnerExceptionLevel=6:innerFormat=Type,Message,ShortType,ToString,Method,Data,StackTrace}" />
  <variable name="file_name" value="${feature}-${instance_number}-${sortabledate}" />
  <variable name="logdir" value="${basedir}/../logs" />
  <variable name="no_newlines" value="${replace:searchFor=\\r\\n|\\n:wholeWords=false:replaceWith=\\n:ignoreCase=true:regex=true:inner=${standard_layout}}"/>
  <variable name="safe_message" value="${replace:inner=${message}:searchFor=[`]:replaceWith=[backtick!]:regex=true}" />
  <variable name="sortabledate" value="${date:format=yyyy-MM-dd-HH}" />
  <variable name="standard_layout" value="${longdate} ${pad:padding=5:inner=${level:uppercase=true}} ${threadid} ${logger} ${safe_message} ${exception_summary}"/>

  <targets async="true">
    <target xsi:type="Null" name="drop" />

    <target xsi:type="Console" name="console" layout="${standard_layout} ${exceptions_detail}" />

    <target name="logstashcsv"
            xsi:type="File"
            encoding="utf-8"
            fileCode="${logdir}/csv/${file_name}.csv"
            archiveFileCode="${logdir}/csv/${file_name}.csv"
            archiveEvery="Hour"
            maxArchiveFiles="8784"
            concurrentWrites="false"
            keepFileOpen="false">
      <layout xsi:type="CSVLayout" withHeader="true" quoting="All" delimiter="Space" quoteChar="`">
        <column name="Time" layout="${longdate}" />
        <column name="Level" layout="${level}"/>
        <column name="Logger" layout="${logger}"/>
        <column name="Message" layout="${replace:inner=${message}:searchFor=\\r\\n|\\n:replaceWith=~~~~~:regex=true}" />
        <column name="Exception" layout="${replace:inner=${exceptions_detail}:searchFor=\\r\\n|\\n:replaceWith=~~~~~:regex=true}" />
        <column name="Request" layout="${RequestHeader:item=x-je-request}" />
        <column name="Conversation" layout="${RequestHeader:item=x-je-conversation}" />
        <column name="Session" layout="${RequestHeader:item=x-je-session}" />
        <column name="User" layout="${RequestHeader:item=x-je-user}" />
        <column name="HttpMethod" layout="${CallInfo:item=HttpMethod}" />
        <column name="PathAndQuery" layout="${CallInfo:item=PathAndQuery}" />
        <column name="Headers" layout="${replace:inner=${CallInfo:item=Headers}:searchFor=\\r\\n|\\n:replaceWith=~~~~~:regex=true}" />
        <column name="Payload" layout="${replace:inner=${CallInfo:item=Payload}:searchFor=\\r\\n|\\n:replaceWith=~~~~~:regex=true}" />
        <column name="ProcessId" layout="${processid}"/>
        <column name="ThreadId" layout="${threadid}"/>
      </layout>
    </target>


  </targets>

  <rules>


    <logger name="*" minlevel="Trace" writeTo="console" />
    <logger name="*" minlevel="Trace" writeTo="logstashcsv" />
  </rules>
</nlog>

