﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Principal;
using BryanAir.Contracts.Helpers;

namespace BryanAir.Web.DomainModel
{
    public class Flight
    {
        public int Id { get; set; }

        /*****
         * o add/cancel/list flights with the following information:
            airline code/name, 
            flight number, //The flight number is a 3-digit number, prefixed with 0’s if the actual number is less than 100
         * departure location, 
         * departure day of the  week/time, 
         * arrival location, 
         * arrival day of the week/time, 
         * cost of business class, 
         * and cost of economy class ticket. . For the simplicity reasons, it is predefined that there are
10 seats of business class and 90 seats of economy class seats in each flight
         * 
         * **/

        public string AirlineCode { get; set; } // see IATA  https://en.wikipedia.org/wiki/List_of_airline_codes
        public string AirlineName { get; set; } // see IATA  https://en.wikipedia.org/wiki/List_of_airline_codes
      
        public int FlightNumber { get; set; } //a 3-digit number, prefixed with 0’s  (Use ViewModel to display etrx digits)

        public string DepartureLocation { get; set; }
        public int DepartureDayOfTheWeek { get; set; } 
        public TimeSpan DepartureTime { get; set; }

        public string ArrivalLocation { get; set; }
        public int ArrivalDayOfTheWeek { get; set; }
        public TimeSpan ArrivalTime { get; set; }

        public decimal FatCatClassTicketPrice { get; set; }
        public decimal CattleClassTicketPrice { get; set; } // Hie Hie Hie

        public int Status { get; set; } // 0 = normal ; 1 = cancelled ... 2 = delayed etc. (should be an enum really.. think MVP!!)

        public DateTime Created { get; set; }

    }


    public class Booking
    {
        public int Id { get; set; }
        public Flight Flight { get; set; }
        public DateTime DepartureDate { get; set; } //ToDo: Really need this?
        public int TotalPassengers { get; set; }
        public decimal Price { get; set; }
        public FlightDirection Direction { get; set; }
        public PriceLevel PriceLevel { get; set; }

    }

    public class Itinerary
    {
        public int Id { get; set; }
        public int Status { get; set; }
        public Guid ReferenceId { get; set; }

        //h itinerary (reserved, booked, or cancelled).
        // flights 
        virtual public IList<Booking> Bookings {get; set;}

        /*
         * . It has the status reserved when it is created by a traveller, booked
            when it is paid, and cancelled when it is deleted by the traveller from his/her itinerary
            list, or one of the flights in the itinerary becomes cancelled by the reservation manager,
            or payment not received after 2 minutes (simulating the 24 hours holding period in the
            real world).

         */

    }

    /***
     * This could be nicer...
     */
    public class UserParticulars
    {
        public int Id { get; set; }
        [Index(IsUnique = true)]
        public Guid ReferenceId { get; set; } // 'Loosely coupled' Client is responsible for providing this in creation & lookup (Add Unique constraint - watchout for EF MySQL generated grabage)

        // for simplicity, CardNumber and ExpirationDate only + a simple address
        public string Name { get; set; }
        public string CreditCardNumber { get; set; } // 16 digits
        public string CreditCardExpirationDate { get; set; }
        public string AddressLine1 { get; set; }    
        public string AddressLine2 { get; set; }
        public string Postcode { get; set; }

         /*The credit card information has to be validated first before booking. As a simplicity
            for this project, a valid combination of credit card number and expiration date is
            defined in such a way that the 4-digit expiration date is defined with a val#1# */
    }


    // Tickets : Generated


        //public string Address { get; set; } // single String vs Address entity ?


}
