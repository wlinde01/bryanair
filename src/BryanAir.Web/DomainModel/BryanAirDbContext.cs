﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Web;
using MySql.Data.Entity;



namespace BryanAir.Web.DomainModel
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class BryanAirDbContext : DbContext
    {
        public DbSet<Flight> Flights { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Itinerary> Itineraries { get; set; }
        public DbSet<UserParticulars> UserParticulars { get; set; }
        //public DbSet<User> Users { get; set; }

        public BryanAirDbContext() : base("name=DefaultConnectionWebService") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }


        


        


    }
}
