using System.Reflection;
using System.Web.Http;

namespace BryanAir.Web.Health
{
    public class HealthController : ApiController
    {
        [HttpGet]
        [Route("health/check")]
        public IHttpActionResult GetHealthCheck()
        {
            return Ok(Assembly.GetExecutingAssembly().GetName().Version.ToString());
        }

        [HttpGet]
        [Route("health/validate")]
        public IHttpActionResult GetDeploymentValidationCheck()
        {
            return Ok(Assembly.GetExecutingAssembly().GetName().Version.ToString());
        }
    }
}
