using System.Data.Entity.Migrations.Design;
using System.Data.Entity.Migrations.Model;
using System.Data.Entity.Migrations.Utilities;
using BryanAir.Contracts.Helpers;
using BryanAir.Web.DomainModel;
using MySql.Data.Entity;

namespace BryanAir.Web.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    

    internal sealed class Configuration : DbMigrationsConfiguration<BryanAir.Web.DomainModel.BryanAirDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(BryanAir.Web.DomainModel.BryanAirDbContext context)
        {
            //context.UserParticulars.AddOrUpdate(new DomainModel.UserParticulars {CreditCardExpirationDate = "12\\2", ReferenceId = 1, CreditCardNumber = "4444444444444441"});

            context.Flights.AddOrUpdate(new Flight
            {
                Id = 20,
                AirlineCode = "XX",
                Created = DateTime.Now,
                Status = (int)Status.New,
                FlightNumber = 666,
                CattleClassTicketPrice = 2000.56m,
                ArrivalLocation = "Hell",
                DepartureDayOfTheWeek = (int) DayOfWeek.Friday,
                DepartureLocation = "London",
                ArrivalDayOfTheWeek = (int) DayOfWeek.Saturday,
                ArrivalTime = new TimeSpan(12, 12, 0),
                DepartureTime = new TimeSpan(13, 10, 0),
                AirlineName = "EasyJet",
                FatCatClassTicketPrice = 0.0m
            });

            context.Flights.AddOrUpdate(new Flight
            {
                Id = 21,
                AirlineCode = "ZY",
                Created = DateTime.Now,
                Status = (int)Status.New,
                FlightNumber = 123,
                CattleClassTicketPrice = 200.56m,
                ArrivalLocation = "Johannesburg",
                DepartureDayOfTheWeek = (int)DayOfWeek.Friday,
                DepartureLocation = "London",
                ArrivalDayOfTheWeek = (int)DayOfWeek.Saturday,
                ArrivalTime = new TimeSpan(12, 12, 0),
                DepartureTime = new TimeSpan(13, 10, 0),
                AirlineName = "EasyJet",
                FatCatClassTicketPrice = 400.0m
            });

            context.Flights.AddOrUpdate(new Flight
            {
                Id = 22,
                AirlineCode = "QT",
                Created = DateTime.Now,
                Status = (int) Status.New,
                FlightNumber = 321,
                CattleClassTicketPrice = 200.56m,
                ArrivalLocation = "London",
                DepartureDayOfTheWeek = (int)DayOfWeek.Saturday,
                DepartureLocation = "Johannesburg",
                ArrivalDayOfTheWeek = (int)DayOfWeek.Sunday,
                ArrivalTime = new TimeSpan(12, 12, 0),
                DepartureTime = new TimeSpan(14, 11, 0),
                AirlineName = "EasyJet",
                FatCatClassTicketPrice = 400.0m
            });


            context.Flights.AddOrUpdate(new Flight
            {
                Id = 23,
                AirlineCode = "BA",
                Created = DateTime.Now,
                Status = (int)Status.New,
                FlightNumber = 123,
                CattleClassTicketPrice = 150.60m,
                ArrivalLocation = "New York",
                DepartureDayOfTheWeek = (int)DayOfWeek.Monday,
                DepartureLocation = "London",
                ArrivalDayOfTheWeek = (int)DayOfWeek.Monday,
                ArrivalTime = new TimeSpan(20, 15, 0),
                DepartureTime = new TimeSpan(12, 12, 0),
                AirlineName = "British Airways",
                FatCatClassTicketPrice = 550.5m
            });


/*
            context.Flights.AddOrUpdate(
                f => f.Id,
                new Flight {AirlineCode = "T1", Created = DateTime.Now, Status = (int)Status.New },
                new Flight { AirlineCode = "T2", Created = DateTime.Now, Status = (int)Status.Cancelled }

                );

*/

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
