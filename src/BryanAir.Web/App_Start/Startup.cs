using System.Configuration;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using BryanAir.Web.Flights;
using BryanAir.Web;
using BryanAir.Web.Itineraries;
using BryanAir.Web.Search;
using BryanAir.Web.UserParticulars;
using Microsoft.Owin;
using Owin;
using StructureMap;
using StructureMap.Configuration.DSL;

[assembly: OwinStartup(typeof(Startup))]

namespace BryanAir.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            Configure(app, CreateContainer());
        }

        public static void Configure(IAppBuilder app, Container container)
        {
            var config = new HttpConfiguration();
            config.Services.Replace(typeof(IHttpControllerActivator), new ServiceActivator(config, container));

            config.MapHttpAttributeRoutes();
            app.UseWebApi(config);
        }

        public static Container CreateContainer()
        {
            return new Container(register =>
            {
                register.AddRegistry<ConfigurationSettingsRegistry>();
            });
        }

    }

    public class ConfigurationSettingsRegistry : Registry
    {
        public ConfigurationSettingsRegistry()
        {
            For<string>().Add(ConfigurationManager.AppSettings["feature"]).Named("feature");
            For<IFlightService>().Use(() => new FlightService());
            For<IUserParticularService>().Use(() => new UserParticularService());
            For<IItineraryService>().Use(() => new ItineraryService());
            For<ISearchService>().Use(() => new SearchService());
        }
    }

    
}
