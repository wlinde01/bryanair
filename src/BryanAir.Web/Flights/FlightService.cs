using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using BryanAir.Web.DomainModel;
using Flight = BryanAir.Contracts.Flight;
using Status = BryanAir.Contracts.Helpers.Status;

namespace BryanAir.Web.Flights // ToDo: Name Space sanitation
{
    public class FlightService : IFlightService
    {
        private readonly BryanAirDbContext _dbContext = new BryanAirDbContext();
        public Flight GetFlight(int id)
        {
            var flight = _dbContext.Flights.FindAsync(id);
            return MapFlightModelToContract(flight.Result);
        }

        public IEnumerable<Flight> GetFlights()
        {
            var flights = _dbContext.Flights.ToListAsync();
            return flights.Result.Select(MapFlightModelToContract);
        }

        public async Task<bool> SaveChanges(Flight flight)
        {
            // The later the hour, the bigger the hack:
            // If a flight gets cancelled, all related Itineraries should also be cancelled...

            _dbContext.Entry(MapContractToModel(flight)).State = EntityState.Modified;
            HackToCancelRelatedBookedFlights(flight);
            var i = await _dbContext.SaveChangesAsync();
            return i > 0; // More than Zero things have been updated.
        }

        public async Task<bool> CreateFlight(Flight flight)
        {
            _dbContext.Flights.Add(MapContractToModel(flight));
            var i = await _dbContext.SaveChangesAsync();
            return i > 0; // Somthing has been added to the DB
        }

        private void HackToCancelRelatedBookedFlights(Flight flight)
        {
            var itineraries = _dbContext.Itineraries
                .Where(i => i.Status == (int)Status.Reserved || i.Status == (int)Status.New)
                .Include(i => i.Bookings.Select(b => b.Flight));

            foreach (var itinerary in itineraries)
            {
                var affected = itinerary.Bookings.Aggregate(false, (current, booking) => current || booking.Flight.Id == flight.Id);
                if (affected) itinerary.Status = (int) Status.Cancelled;
            }


            // Get all bookings, for this flight that has not been confirmed and cancel them
            //

        }


        public static Flight MapFlightModelToContract(DomainModel.Flight flight)
        {
            return new Flight
            {
                AirlineCode = flight.AirlineCode,
                AirlineName = flight.AirlineName,
                ArrivalDayOfTheWeek =  flight.ArrivalDayOfTheWeek,
                ArrivalLocation =  flight.ArrivalLocation,
                ArrivalTime = flight.ArrivalTime,
                CattleClassTicketPrice = flight.CattleClassTicketPrice,
                Created = flight.Created,
                DepartureDayOfTheWeek = flight.DepartureDayOfTheWeek,
                DepartureLocation = flight.DepartureLocation,
                DepartureTime = flight.DepartureTime,
                Id = flight.Id,
                Status = (Status) flight.Status,
                FatCatClassTicketPrice = flight.FatCatClassTicketPrice,
                FlightNumber = flight.FlightNumber
            };
        }


        //ToDo: this is nasty, use a mapper, or at least ellevate mappers to Extensions.. if there is time.
        public static DomainModel.Flight MapContractToModel(Flight flight)
        {
            return new DomainModel.Flight
            {
                AirlineCode = flight.AirlineCode,
                AirlineName = flight.AirlineName,
                ArrivalDayOfTheWeek = flight.ArrivalDayOfTheWeek,
                ArrivalLocation = flight.ArrivalLocation,
                ArrivalTime = flight.ArrivalTime,
                CattleClassTicketPrice = flight.CattleClassTicketPrice,
                Created = flight.Created,
                DepartureDayOfTheWeek = flight.DepartureDayOfTheWeek,
                DepartureLocation = flight.DepartureLocation,
                DepartureTime = flight.DepartureTime,
                Id = flight.Id,
                Status = (int) flight.Status,
                FatCatClassTicketPrice = flight.FatCatClassTicketPrice,
                FlightNumber = flight.FlightNumber
            };
        }
    }
}
