using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using BryanAir.Contracts;

namespace BryanAir.Web.Flights
{
    public class FlightController : ApiController
    {
        private readonly IFlightService _flightService;


        public FlightController(IFlightService flightService)
        {
            _flightService = flightService;
        }

        [HttpGet]
        [Route("api/flights/{id}")]
        public IHttpActionResult GetFlight(int id)
        {
            var flight = _flightService.GetFlight(id);
            return Ok(flight);
        }

        [HttpGet]
        [Route("api/flights")]
        public IHttpActionResult GetFlights()
        {
            var flight = _flightService.GetFlights();
            return Ok(flight.ToList());
        }

        [HttpPut]
        [Route("api/flights")]
        public IHttpActionResult Update(Flight flight)
        {
            if (_flightService.SaveChanges(flight).Result) return Ok(flight);
            return BadRequest(); // ToDo: InternalServerError??
        }

        [HttpPost]
        [Route("api/flights")]
        public IHttpActionResult Create(Flight flight)
        {
            if (_flightService.CreateFlight(flight).Result) return Ok(flight);
            return BadRequest(); // ToDo: InternalServerError??
        }

    }
}
