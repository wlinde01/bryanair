using System.Collections.Generic;
using System.Threading.Tasks;
using BryanAir.Contracts;

namespace BryanAir.Web.Flights
{
    public interface IFlightService
    {
        Flight GetFlight(int id);
        IEnumerable<Flight> GetFlights();
        Task<bool> SaveChanges(Flight flight);
        Task<bool> CreateFlight(Flight flight);
    }
}
