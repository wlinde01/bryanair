using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using BryanAir.Contracts;
using BryanAir.Web.DomainModel;

namespace BryanAir.Web.UserParticulars
{
    public class UserParticularController : ApiController
    {
        private readonly IUserParticularService _userParticularService;


        public UserParticularController(IUserParticularService userParticularService)
        {
            _userParticularService = userParticularService;
        }



        [HttpPost]
        [Route("api/userparticulars/")]
        public IHttpActionResult CreateParticular(UserParticular userParticular)
        {
            if (_userParticularService.AddParticular(userParticular).Result) return Ok(userParticular);
            return BadRequest(); // ToDo: InternalServerError??
        }


        [HttpGet]
        [Route("api/userparticulars/{userId}")]
        public IHttpActionResult GetUserParticulars(Guid userId)
        {
            var particular = _userParticularService.GetParticular(userId);
            if (particular == null) return NotFound();
            return Ok(particular);
        }

        [HttpPut]
        [Route("api/userparticulars")]
        public IHttpActionResult UpdateCreditCardParticulars(UserParticular creditCardparticulars)
        {
            var particular = _userParticularService.UpdateCreditcardParticular(creditCardparticulars);
            if (particular == null) return NotFound();
            return Ok(particular);
        }


/*
        [HttpPost]
        [Route("api/user/creditcard")]
        public IHttpActionResult CreateCreditCard(UserCreditCard creditCard)
        {
            if (_userParticularService.AddCreditCard(creditCard).Result) return Ok(creditCard);
            return BadRequest(); // ToDo: InternalServerError??
        }


        [HttpGet]
        [Route("api/user/creditcard/{id}")]
        public IHttpActionResult GetCreditCard(int id)
        {
            var creditCard = _userParticularService.GetCreditCard(id);
            if (creditCard == null) return NotFound();
            return Ok(creditCard);
        }

        [HttpGet]
        [Route("api/user/address/{userId}")]
        public IHttpActionResult GetAddress(int userId)
        {
            var address = _userParticularService.GetAddress(userId);
            if (address == null) return NotFound();
            return Ok(address);
        }

        [HttpPost]
        [Route("api/user/address")]
        public IHttpActionResult CreateAddress(UserAddress address)
        {
            if (_userParticularService.AddAddress(address).Result) return Ok(address);
            return BadRequest(); // ToDo: InternalServerError??
        }*/

    }


}
