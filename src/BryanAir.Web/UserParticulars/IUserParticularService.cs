using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using BryanAir.Contracts;
using BryanAir.Web.DomainModel;

namespace BryanAir.Web.UserParticulars
{
    public interface IUserParticularService
    {
        Task<bool> AddParticular(UserParticular userParticular);
        UserParticular GetParticular(Guid referenceId);
        Task<UserParticular> UpdateCreditcardParticular(UserParticular creditCardParticular);
    }
    public class UserParticularService : IUserParticularService
    {
        private readonly BryanAirDbContext _dbContext = new BryanAirDbContext();

        public async Task<bool> AddParticular(UserParticular userParticular)
        {
            var particular = _dbContext.UserParticulars.Add(MapContractToModelUserParticular(userParticular));
            var i = await _dbContext.SaveChangesAsync();
            return i > 0;
        }

        public async Task<UserParticular> UpdateCreditcardParticular(UserParticular creditCardParticular)
        {
            var particularToUpdate = _dbContext.UserParticulars.Find(creditCardParticular.Id);
            particularToUpdate.CreditCardNumber = creditCardParticular.CreditCardNumber;
            particularToUpdate.CreditCardExpirationDate = creditCardParticular.CreditCardExpirationDate;

            _dbContext.Entry(particularToUpdate).State = EntityState.Modified;
            var i = await _dbContext.SaveChangesAsync();
            return i <= 0 ? null : creditCardParticular;
        }


        public UserParticular GetParticular(Guid referenceId)
        {
            var particular = _dbContext.UserParticulars.ToListAsync().Result.FirstOrDefault(p => p.ReferenceId == referenceId);
            if (particular == null) return null;
            return new UserParticular
            {
                Id = particular.Id,
                ReferenceId = particular.ReferenceId,
                Name = particular.Name,
                AddressLine1 = particular.AddressLine1,
                AddressLine2 = particular.AddressLine2,
                Postcode = particular.Postcode,
                CreditCardNumber = particular.CreditCardNumber,
                CreditCardExpirationDate = particular.CreditCardExpirationDate
            };
        }

        private static DomainModel.UserParticulars MapContractToModelUserParticular(UserParticular particular)
        {
            return new DomainModel.UserParticulars
            {
                Id = particular.Id,
                ReferenceId = particular.ReferenceId,
                Name = particular.Name,
                CreditCardNumber = particular.CreditCardNumber,
                CreditCardExpirationDate = particular.CreditCardExpirationDate,
                AddressLine1 = particular.AddressLine1,
                AddressLine2 = particular.AddressLine2,
                Postcode = particular.Postcode
            };

        }


        
    }

}
