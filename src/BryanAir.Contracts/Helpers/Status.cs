namespace BryanAir.Contracts.Helpers
{
    public enum Status
    {
        New,
        Cancelled,
        Reserved,
        Booked,
        Expired,
    }

    public enum PriceLevel
    {
        Economy,
        Bussiness
    }

    public enum FlightDirection
    {
        Inbound,
        Outbound
    }
}
