using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BryanAir.Contracts.Helpers;

namespace BryanAir.Contracts
{
    public class Flight
    {
        public int Id { get; set; }
        public string AirlineCode { get; set; }
        public string AirlineName { get; set; }
        public int FlightNumber { get; set; }
        public string DepartureLocation { get; set; }
        public int DepartureDayOfTheWeek { get; set; }
        public TimeSpan DepartureTime { get; set; }
        public string ArrivalLocation { get; set; }
        public int ArrivalDayOfTheWeek { get; set; }
        public TimeSpan ArrivalTime { get; set; }
        public decimal FatCatClassTicketPrice { get; set; }
        public decimal CattleClassTicketPrice { get; set; } // Hie Hie Hie
        public Status Status { get; set; } // 0 = normal ; 1 = cancelled ... 2 = delayed etc. (should be an enum really.. think MVP!!)
        public DateTime Created { get; set; }
        public int BookedSeatsEco { get; set; }
        public int BookedSeatsBussiness { get; set; }
    }
}
