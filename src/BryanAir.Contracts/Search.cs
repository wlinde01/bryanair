﻿using System;
using System.Collections.Generic;

namespace BryanAir.Contracts
{
    public class SearchRequest
    {
        public string DepartureLocation { get; set; }
        public DateTime DepartureTime { get; set; }
        public bool IsReturnFlight { get; set; }
        public string ArrivalLocation { get; set; }

        public int TotalPassengers { get; set; }
        public DateTime ReturnTime { get; set; }
    }

    public class SearchResponse
    {
        //ToDo: Add outbound & Inbound
        public IList<Flight> OutboundFlights { get; set; }
        public IList<Flight> InboundFlights { get; set; }
    }
}
