﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BryanAir.Contracts
{
    public class UserParticular
    {
        public int Id { get; set; }
        public Guid  ReferenceId { get; set; }
        public string Name { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Postcode { get; set; }
        public string CreditCardNumber { get; set; } // 16 digits
        public string CreditCardExpirationDate { get; set; }
    }





}
