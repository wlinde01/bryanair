﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BryanAir.Contracts.Helpers;

namespace BryanAir.Contracts
{
    public class Itinerary
    {
        public int Id { get; set; }
        public int Status { get; set; }
        public Guid ReferenceId { get; set; }
        public IList<Booking> Bookings { get; set; }
    }


    public class Booking
    {
        public int Id { get; set; }
        public Flight Flight { get; set; }
        public DateTime DepartureDate { get; set; }
        public int TotalPassengers { get; set; }
        public decimal Price { get; set; }
        public FlightDirection Direction { get; set; }
        public PriceLevel PriceLevel { get; set; }
    }

    public class NewBookingRequest
    {
        public int FlightId { get; set; }
        public DateTime DepartureDate { get; set; }
        public int TotalPassengers { get; set; }
        public PriceLevel PriceLevel { get; set; }
        public FlightDirection Direction { get; set; }
    }

}
