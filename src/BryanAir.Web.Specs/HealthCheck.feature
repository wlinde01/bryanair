Feature: HealthCheck
	In order to include service in ELB
	As a BryanAir.Web instance
	I want to return a healthy response to the health URL

@health
Scenario: Can return healthy
	When I request the healthcheck
	Then the response should be a 200
