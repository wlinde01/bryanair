using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FakeItEasy;
using FakeItEasy.ExtensionSyntax.Full;
using BryanAir.Contracts;
using BryanAir.Web.Flights;
using Ploeh.AutoFixture;
using Shouldly;
using TechTalk.SpecFlow;
using Newtonsoft.Json;

namespace BryanAir.Web.Specs
{
    [Binding]
    public class FlightSteps
    {
        private readonly ServerContext _serverContext;
        private HttpResponseMessage _response;

        public FlightSteps(ServerContext serverContext)
        {
            _serverContext = serverContext;
        }

        [Given(@"I have a sample service with:")]
        public void GivenIHaveASampleServiceWith(Table table)
        {
            var service = _serverContext.Fixture.Freeze<IFlightService>();

            foreach (var row in table.Rows)
            {
                var id = int.Parse(row["Id"]);
                var code = int.Parse(row["Code"]); // ToDo consider this a string

                service.CallsTo(x => x.GetFlight(id)).Returns(new Flight() {Id = id, FlightNumber = code});
            }

            _serverContext.Container.Configure(c => c.For<IFlightService>().Use(() => service));

        }

        [When(@"I request flight id (.*)")]
        public void WhenIRequestFlightId(int id)
        {
            _response = _serverContext.Server.HttpClient.GetAsync(string.Format("api/flights/{0}", id)).Result;
            var responseContent = _response.Content.ReadAsStringAsync().Result;
            // ToDo think how to deserialise within service
            // var _flightResponse = JsonConvert.DeserializeAnonymousType(responseContent, typeof(Flight)); //<Flight>();
            // var x = _flightResponse.ToString();
        }

        [Then(@"the response should be (.*)")]
        public void ThenTheResponseShouldBe(int status)
        {
            _response.StatusCode.ShouldBe((HttpStatusCode)status);
        }

        [Then(@"the code should be (.*)")]
        public void ThenTheCodeShouldBe(int code)
        {
            ScenarioContext.Current.Pending();
        }


/*
        // Dump or Redo Propperly
        [Then(@"I create a card and it works")]
        public void ThenICreateACardAndItWorks()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:34015/");
                var response = client.PostAsJsonAsync("api/user/creditcard", 
                    new UserCreditCard { Id = 2, CreditCardExpirationDate = "10\\2", ReferenceId = 12, CreditCardNumber = "4444444444444444" }).Result;
                //if (!response.IsSuccessStatusCode) { throw new Exception(response.ReasonPhrase);}
                
                response.StatusCode.ShouldBe(HttpStatusCode.OK);
            }


            ScenarioContext.Current.Pending();
        }
*/


    }
}
