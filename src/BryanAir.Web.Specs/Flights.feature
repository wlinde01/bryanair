Feature: Flights
	In order to use the Flight API endpoints
	As an API developer
	I want to have tests for my flight service

@flight
Scenario: lookup a flight
	Given I have a sample service with:
  | Id | Code      |
  | 1  | 066       |
	When I request flight id 1
	Then the response should be 200
  #And the code should be 006


#Scenario: Create a creditcard
#Then I create a card and it works
