using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Shouldly;
using TechTalk.SpecFlow;

namespace BryanAir.Web.Specs
{
    [Binding]
    public class HealthCheckSteps
    {
        private readonly ServerContext _serverContext;
        private Task<HttpResponseMessage> _response;

        public HealthCheckSteps(ServerContext serverContext)
        {
            _serverContext = serverContext;
        }

        [When(@"I request the healthcheck")]
        public void WhenIRequestTheHealthcheck()
        {
            _response = _serverContext.Server.HttpClient.GetAsync("/health/check");
        }

        [Then(@"the response should be a (.*)")]
        public void ThenTheResponseShouldBeA(int status)
        {
            _response.Result.StatusCode.ShouldBe((HttpStatusCode) status);
        }
    }
}
