using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BryanAir.Web;
using Microsoft.Owin.Testing;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoFakeItEasy;
using StructureMap;

namespace BryanAir.Web.Specs
{
    public class ServerContext
    {
        private readonly Lazy<TestServer> _testServer;

        public Container Container { get; private set; }
        public Fixture Fixture;

        public ServerContext()
        {
            Container = Startup.CreateContainer();

            Fixture = new Fixture();
            Fixture.Customize(new AutoFakeItEasyCustomization());

            _testServer = new Lazy<TestServer>(CreateServer);
        }

        public TestServer Server
        {
            get { return _testServer.Value; }
        }


        public TestServer CreateServer()
        {
            return TestServer.Create(app => Startup.Configure(app, Container));
        }

    }
}
